package net.agilob;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestComparingBooleanBoxes {

    private static final String abcString = "abc";

    @Test
    public void testComparingBooleans() {
        Assertions.assertTrue(true == true);

        Boolean b1 = Boolean.TRUE;
        Boolean b2 = Boolean.TRUE;
        Assertions.assertTrue(b1 == b2);

        Boolean boxedB1 = new Boolean(true);
        Boolean boxedB2 = new Boolean(true);
        Assertions.assertFalse(boxedB1 == boxedB2);
        Assertions.assertEquals(boxedB1, boxedB2);

        Object ob1 = new Boolean(true);
        Object ob2 = new Boolean(true);
        Assertions.assertFalse(ob1 == ob2);
        Assertions.assertEquals(ob1, ob2);
    }

    @Test
    public void testComparingStrings() {
        Assertions.assertTrue("abc" == "abc");

        String s1 = "abc";
        String s2 = "abc";
        Assertions.assertTrue(s1 == s2);
        Assertions.assertEquals(s1, s2);

        String boxedB1 = new String("abc");
        String boxedB2 = new String("abc");
        Assertions.assertFalse(boxedB1 == boxedB2);
        Assertions.assertEquals(boxedB1, boxedB2);

        Object ob1 = new String("abc");
        Object ob2 = new String("abc");
        Assertions.assertFalse(ob1 == ob2);
        Assertions.assertEquals(ob1, ob2);

        Object ob3 = new String(abcString);
        Object ob4 = new String(abcString);
        Assertions.assertFalse(ob3 == ob4);
        Assertions.assertEquals(ob3, ob4);

        String static1 = abcString;
        String static2 = abcString;
        Assertions.assertTrue(static1 == static2);
        Assertions.assertEquals(static1, static2);

        Assertions.assertTrue("abc" == abcString);
        Assertions.assertTrue("a" + "b" + "c" == "abc");
    }
}
