package net.agilob;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class FactorialTest {

    @ParameterizedTest
    @ValueSource(ints = { 2, 4 })
    long factorial(int n) {
        if(n<2) {
            return n;
        }
        return n * factorial(n-1);
    }
}
