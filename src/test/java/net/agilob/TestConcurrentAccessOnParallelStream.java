package net.agilob;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.LongStream;

public class TestConcurrentAccessOnParallelStream {

    private Long counter = 0L;

    private volatile long volatileCounter = 0L;

    private AtomicLong atomicLong = new AtomicLong(0L);

    private volatile long value = 0;

    private Boolean locker = new Boolean(true);

    private long getValue() {
        synchronized(locker) {
            return value;
        }
    }

    private void setValue(long newValue) {
        synchronized(locker) {
            this.value = newValue;
        }
    }

    @Test
    public void testAccessOnStream() {
        counter = 0L;

        Long sum = LongStream.range(0L, 100L).map(
                l -> {
                    counter = counter + l;
                    return counter;
                }).sum();
        Assertions.assertEquals(166_650L, sum.longValue());
    }

    @Test
    public void testConcurrentAccessOnParallelStreamWithLocks() {
        setValue(0L);
        Long sum = LongStream.range(0L, 100L).parallel().map(
                l -> {
                    setValue(counter + l);
                    return getValue();
                }).sum();
        Assertions.assertNotEquals(166_650L, sum.longValue());
    }

    @Test
    public void testConcurrentAccessOnParallelStreamWithAtomicLong() {
        atomicLong.set(0L);
        Long sum = LongStream.range(0L, 100L).parallel().map(
                l -> {
                    atomicLong.set(atomicLong.get() + l);
                    return atomicLong.get();
                }).sum();
        Assertions.assertNotEquals(166_650L, sum.longValue());
    }

    @Test
    public void testConcurrentAccessOnParallelStreamWithVolatile() {
        volatileCounter = 0L;
        Long sum = LongStream.range(0L, 100L).parallel().map(
                l -> {
                    volatileCounter = volatileCounter + l;
                    return volatileCounter;
                }).sum();
        Assertions.assertNotEquals(166_650L, sum.longValue());
    }

    @Test
    public void testConcurrentAccessOnParallelStream() {
        counter = 0L;
        Long sum = LongStream.range(0L, 100L).parallel().map(
                l -> {
                    counter = counter + l;
                    return counter;
                }).sum();
        Assertions.assertNotEquals(166_650L, sum.longValue());
    }

    @Test
    public void testAccessOnStreamPeek() {
        counter = 0L;

        Long sum = LongStream.range(0L, 100L).peek(
                l -> {
                    counter = counter + l;
                }).sum();
        Assertions.assertNotEquals(166_650L, counter.longValue());
    }

    @Test
    public void testConcurrentAccessOnParallelStreamWithForkJoinPool()
            throws ExecutionException, InterruptedException {
        counter = 0L;

        ForkJoinPool forkJoinPool = new ForkJoinPool(8);
        Long sum = forkJoinPool.submit(()->
            LongStream.range(0L, 100L).parallel().map(
                    l -> {
                        counter = counter + l;
                        return counter;
                }).sum()
        ).get();
        Assertions.assertNotEquals(166_650L, sum.longValue());
    }
}
