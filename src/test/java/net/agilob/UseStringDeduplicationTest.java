package net.agilob;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UseStringDeduplicationTest {
    private String s1 = "test";
    private static final String s2 = "test";
    private String s3 = new String("test");

    @Test
    public void compareString() {
        assertTrue("test" == s1);
        assertTrue("test" == s2);
//        assertTrue("test" == s3);
        assertTrue("test".equals(s1));
        assertTrue(s1.equals(s2));
        assertTrue(s1.equals(s3));
        assertTrue(s2.equals(s3));


    }
}
