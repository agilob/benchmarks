package net.agilob.vectors;

import jdk.incubator.vector.FloatVector;
import jdk.incubator.vector.VectorSpecies;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@Fork(value = 1)
@Warmup(iterations = 1, timeUnit = TimeUnit.SECONDS, time = 2)
@Measurement(iterations = 3)
public class ScalarMultiplication {

    private static final float[] a = new float[1000];
    private static final float[] b = new float[1000];
    private static final float[] c = new float[1000];

    static {
        for (int i = 0; i < 1000; i++) {
            a[i] = ThreadLocalRandom.current().nextFloat();
            b[i] = ThreadLocalRandom.current().nextFloat();
            c[i] = ThreadLocalRandom.current().nextFloat();
        }
    }

    @Benchmark
    public void scalarComputation(Blackhole blackhole) {
        for (int i = 0; i < a.length; i++) {
            c[i] = (a[i] * a[i] + b[i] * b[i]) * -1.0f;
        }
        blackhole.consume(c);
    }

    static final VectorSpecies<Float> SPECIES = FloatVector.SPECIES_PREFERRED;

    @Benchmark
    public void vectorComputation(Blackhole blackhole) {
        int i = 0;
        int upperBound = SPECIES.loopBound(a.length);
        for (; i < upperBound; i += SPECIES.length()) {
            // FloatVector va, vb, vc;
            var va = FloatVector.fromArray(SPECIES, a, i);
            var vb = FloatVector.fromArray(SPECIES, b, i);
            var vc = va.mul(va)
                    .add(vb.mul(vb))
                    .neg();
            vc.intoArray(c, i);
        }
        for (; i < a.length; i++) {
            c[i] = (a[i] * a[i] + b[i] * b[i]) * -1.0f;
        }
        blackhole.consume(c);
    }
}
