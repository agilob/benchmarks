package net.agilob.common;

import net.agilob.model.IamEnum;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.util.EnumSet;

@Fork(value = 2)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class EnumsIterate {

    @Benchmark
    public void iterateOnValues(Blackhole blackhole) {
        for (IamEnum value : IamEnum.values()) {
            blackhole.consume(value);
        }
    }

    @Benchmark
    public void iterateOnEnumSet(Blackhole blackhole) {
        for (IamEnum value : EnumSet.allOf(IamEnum.class)) {
            blackhole.consume(value);
        }
    }
}
