package net.agilob.common;

import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;
import org.openjdk.jmh.annotations.*;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
@State(value = Scope.Benchmark)
public class EmptyString {

    private String emptyString = "";

    private String nullString = null;

    private String notEmptyString = "abcd";

    @Benchmark
    public boolean testEmptyStringApache() {
        return StringUtils.isEmpty(emptyString);
    }

    @Benchmark
    public boolean testEmptyStringGuava() {
        return Strings.isNullOrEmpty(emptyString);
    }

    @Benchmark
    public boolean testEmptyStringManual() {
        return emptyString == null || emptyString.isEmpty();
    }

    @Benchmark
    public boolean testEmptyStringManualLength() {
        return nullString == null || nullString.length() == 0;
    }

    ////

    @Benchmark
    public boolean testNullStringApache() {
        return StringUtils.isEmpty(nullString);
    }

    @Benchmark
    public boolean testNullStringGuava() {
        return Strings.isNullOrEmpty(nullString);
    }

    @Benchmark
    public boolean testNullStringManual() {
        return nullString == null || nullString.isEmpty();
    }

    @Benchmark
    public boolean testNullStringManualLength() {
        return nullString == null || nullString.length() == 0;
    }

    ////

    @Benchmark
    public boolean testNotEmptyStringApache() {
        return StringUtils.isEmpty(notEmptyString);
    }

    @Benchmark
    public boolean testNotEmptyStringGuava() {
        return Strings.isNullOrEmpty(notEmptyString);
    }

    @Benchmark
    public boolean testNotEmptyStringManual() {
        return notEmptyString == null || notEmptyString.isEmpty();
    }

    @Benchmark
    public boolean testNotStringManualLength() {
        return nullString == null || nullString.length() == 0;
    }
}
