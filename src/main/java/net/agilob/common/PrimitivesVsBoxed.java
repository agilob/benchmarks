package net.agilob.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ThreadLocalRandom;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class PrimitivesVsBoxed {

    private static final int ITERATIONS = 1_000_000;

    @Benchmark
    public Long addingOnBoxedLongs() {
        Collection<Long> longs = new ArrayList<>(ITERATIONS);
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        for (int i = 0; i < ITERATIONS; i++) {
            longs.add(threadLocalRandom.nextLong());
        }

        Long sum = 0L;
        for (Long mLong : longs) {
            sum = +mLong;
        }
        return sum;
    }

    @Benchmark
    public long addingOnLongs() {
        long[] longs = new long[ITERATIONS];
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        for (int i = 0; i < ITERATIONS; i++) {
            longs[i] = threadLocalRandom.nextLong();
        }

        Long sum = 0L;
        for (long mLong : longs) {
            sum = +mLong;
        }
        return sum;
    }

    @Benchmark
    public BigDecimal addingToBigDecimalBoxed() {
        Collection<Long> longs = new ArrayList<>(ITERATIONS);
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        for (int i = 0; i < ITERATIONS; i++) {
            longs.add(threadLocalRandom.nextLong());
        }

        BigDecimal sum = new BigDecimal(0);
        for (Long mLong : longs) {
            sum = sum.add(new BigDecimal(mLong));
        }
        return sum;
    }

    @Benchmark
    public BigDecimal addingToBigDecimal() {
        long[] longs = new long[ITERATIONS];
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        for (int i = 0; i < ITERATIONS; i++) {
            longs[i] = threadLocalRandom.nextLong();
        }

        BigDecimal sum = new BigDecimal(0);
        for (long mLong : longs) {
            sum = sum.add(new BigDecimal(mLong));
        }
        return sum;
    }

}
