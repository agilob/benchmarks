package net.agilob.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class SingleElementList {

    @Benchmark
    public List<Integer> arraysAsList() {
        return Arrays.asList(ThreadLocalRandom.current().nextInt());
    }

    @Benchmark
    public List<Integer> collectionsSingletonList() {
        return Collections.singletonList(ThreadLocalRandom.current().nextInt());
    }

    @Benchmark
    public List<Integer> newArrayList() {
        List<Integer> list = new ArrayList<>(1);
        list.add(ThreadLocalRandom.current().nextInt());
        return list;
    }

}
