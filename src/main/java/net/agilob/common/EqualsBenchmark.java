package net.agilob.common;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import net.agilob.model.UserNoEquals;
import net.agilob.model.UserWithEquals;
import org.openjdk.jmh.annotations.*;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class EqualsBenchmark {

    @Param({ "10", "100", "1000", "1000000" })
    int size;

    private List<UserWithEquals> usersWithEquals;

    private List<UserNoEquals> usersNoEquals;

    @Setup(Level.Iteration)
    public void setup() {
        usersWithEquals = new ArrayList<>(size);
        usersNoEquals = new ArrayList<>(size);

        for (int i = 0; i < size; i++) {
            UserNoEquals u = new UserNoEquals();
            u.setId(ThreadLocalRandom.current().nextLong());
            u.setUsername(UUID.randomUUID().toString());
            u.setPassword(UUID.randomUUID().toString());
            u.setEmail(UUID.randomUUID().toString());
            u.setRegistrationIp(UUID.randomUUID().toString());
            u.setLastIp(UUID.randomUUID().toString());
            u.setCreationDate(new Date());
            u.setLastLoginDate(new Date());
            usersNoEquals.add(u);

            UserWithEquals u1 = new UserWithEquals();
            u1.setId(ThreadLocalRandom.current().nextLong());
            u1.setUsername(UUID.randomUUID().toString());
            u1.setPassword(UUID.randomUUID().toString());
            u1.setEmail(UUID.randomUUID().toString());
            u1.setRegistrationIp(UUID.randomUUID().toString());
            u1.setLastIp(UUID.randomUUID().toString());
            u1.setCreationDate(new Date());
            u1.setLastLoginDate(new Date());
            usersWithEquals.add(u1);
        }
    }

    @Benchmark
    public List<UserWithEquals> LinkedHashSetWithEquals() {
        return new ArrayList<>(new LinkedHashSet<>(usersWithEquals));
    }

    @Benchmark
    public List<UserWithEquals> streamWithEquals() {
        return usersWithEquals.stream().distinct().collect(Collectors.toList());
    }

    @Benchmark
    public List<UserWithEquals> parallelStreamWithEquals() {
        return usersWithEquals.parallelStream().distinct().collect(Collectors.toList());
    }

    @Benchmark
    public List<UserWithEquals> hashSetWithEquals() {
        return new ArrayList<>(new HashSet<>(usersWithEquals));
    }

    @Benchmark
    public List<UserWithEquals> identityHashSetWithEquals() {
        Set<UserWithEquals> asdas = Collections.newSetFromMap(new IdentityHashMap<>());
        asdas.addAll(usersWithEquals);
        return new ArrayList<>(asdas);
    }

    @Benchmark
    public List<UserNoEquals> LinkedHashSetWithoutEquals() {
        return new ArrayList<>(new LinkedHashSet<>(usersNoEquals));
    }

    @Benchmark
    public List<UserNoEquals> streamWithoutEquals() {
        return usersNoEquals.stream().distinct().collect(Collectors.toList());
    }

    @Benchmark
    public List<UserNoEquals> parallelStreamWithoutEquals() {
        return usersNoEquals.parallelStream().distinct().collect(Collectors.toList());
    }

    @Benchmark
    public List<UserNoEquals> hashSetWithoutEquals() {
        return new ArrayList<>(new HashSet<>(usersNoEquals));
    }

    @Benchmark
    public List<UserNoEquals> identityHashSetWithoutEquals() {
        Set<UserNoEquals> asdas = Collections.newSetFromMap(new IdentityHashMap<>());
        asdas.addAll(usersNoEquals);
        return new ArrayList<>(asdas);
    }

}
