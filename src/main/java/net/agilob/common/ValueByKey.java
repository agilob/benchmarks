package net.agilob.common;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
@State(value = Scope.Benchmark)
public class ValueByKey {

    private Long counter = 0L;

    private static final int MAX = 2500;

    private BiMap<String, Long> guavaBiMap = HashBiMap.create();

    private BidiMap<String, Long> apacheBiDiMap = new DualHashBidiMap<>();

    private final LoadingCache<String, Long> stringToLong = CacheBuilder.newBuilder().concurrencyLevel(4)
            .maximumSize(Math.round(MAX * 1.1)).softValues().expireAfterAccess(32, TimeUnit.SECONDS)
            .build(new CacheLoader<String, Long>() {
                public Long load(String mString) {
                    return generateIdByString(mString);
                }
            });

    private final Map<String, Long> mHashMap = new Hashtable<>(MAX);
    private final Map<String, Long> concurrentHashMap = new ConcurrentHashMap<>(MAX);

    @Setup(Level.Trial)
    public void setup() {
        // Populate guava cache
        for (int i = 0; i <= MAX; i++) {
            try {
                stringToLong.get(UUID.randomUUID().toString());
            } catch (ExecutionException e) {
            }
        }
    }

    @Benchmark
    public String guavaBiMap() {
        Long randomNum = ThreadLocalRandom.current().nextLong(1L, MAX);
        return guavaBiMap.inverse().get(randomNum);
    }

    @Benchmark
    public String apacheBiDiMap() {
        Long randomNum = ThreadLocalRandom.current().nextLong(1L, MAX);
        return apacheBiDiMap.getKey(randomNum);
    }

    @Benchmark
    public String stringToIdByIteration() {
        Long randomNum = ThreadLocalRandom.current().nextLong(1L, MAX);

        for (Map.Entry<String, Long> entry : stringToLong.asMap().entrySet()) {
            if (Objects.equals(randomNum, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    @Benchmark
    public String stringToIdByIterationHashTable() {
        Long randomNum = ThreadLocalRandom.current().nextLong(1L, MAX);

        for (Map.Entry<String, Long> entry : mHashMap.entrySet()) {
            if (Objects.equals(randomNum, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    @Benchmark
    public String stringToIdByIterationConcurrentHashMap() {
        Long randomNum = ThreadLocalRandom.current().nextLong(1L, MAX);

        for (Map.Entry<String, Long> entry : concurrentHashMap.entrySet()) {
            if (Objects.equals(randomNum, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    @Benchmark
    public String stringToIdByStream() {
        long randomNum = ThreadLocalRandom.current().nextLong(1L, MAX);

        return stringToLong.asMap().entrySet().stream().filter(entry -> randomNum == entry.getValue())
                .map(Map.Entry::getKey).findFirst().get();
    }

    @Benchmark
    public String stringToIdByParallelStream() {
        Long randomNum = ThreadLocalRandom.current().nextLong(1L, MAX);

        return stringToLong.asMap().entrySet().parallelStream()
                .filter(entry -> Objects.equals(entry.getValue(), randomNum)).limit(1).map(Map.Entry::getKey)
                .reduce((a, b) -> null).toString();
    }

    private Long generateIdByString(final String mString) {
        counter++;
        mHashMap.put(mString, counter);
        concurrentHashMap.put(mString, counter);
        guavaBiMap.put(mString, counter);
        apacheBiDiMap.put(mString, counter);
        return counter;
    }

}
