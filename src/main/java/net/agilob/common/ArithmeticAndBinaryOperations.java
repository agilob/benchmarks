package net.agilob.common;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 5)
@State(value = Scope.Benchmark)
public class ArithmeticAndBinaryOperations {

  private static final int MAX = 10_000;

  @Benchmark
  public int getCountBinaryShift() {
    int counter = 0;
    for (int i = 0; i < MAX; i++) {
      if ((i & 1) == 0) {
        counter++;
      }
    }
    return counter;
  }

  @Benchmark
  public int getCountModulo() {
    int counter = 0;
    for (int i = 0; i < MAX; i++) {
      if (i % 2 == 0) {
        counter++;
      }
    }
    return counter;
  }

}
