package net.agilob.common;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.CompilerControl;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class InliningBenchmarkThroughput {

    private static final int A = 10;

    private static final int B = 20;

    private static volatile int C = 10;

    private static volatile int D = 10;

    @Benchmark
    public int staticAdderBenchmark() {
        return InliningBenchmarkThroughput.staticAdderMethod(A, B);
    }

    @Benchmark
    public int notInlinedAdder() {
        return this.notInlinedAdderMethod(A, B);
    }

    @Benchmark
    public int excludedAdder() {
        return this.excludedAdderMethod(A, B);
    }

    @Benchmark
    public int inclassAdder() {
        return inclassAdderMethod();
    }

    @Benchmark
    public int volatileStaticAdderBenchmark() {
        return InliningBenchmarkThroughput.staticAdderMethod(C, D);
    }

    @Benchmark
    public int volatileNotInlinedAdder() {
        return this.notInlinedAdderMethod(C, D);
    }

    @Benchmark
    public int volatileExcludedAdder() {
        return this.excludedAdderMethod(C, D);
    }

    @Benchmark
    public int volatileInclassAdder() {
        return volatileInclassAdderMethod();
    }

    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    private int excludedAdderMethod(int a, int b) {
        int sum = 0;
        sum = a + b;
        return sum;
    }

    @CompilerControl(CompilerControl.Mode.DONT_INLINE)
    private int notInlinedAdderMethod(int a, int b) {
        int sum = 0;
        sum = a + b;
        return sum;
    }

    private int inclassAdderMethod() {
        return A + B;
    }

    private int volatileInclassAdderMethod() {
        return C + D;
    }

    private static int staticAdderMethod(int a, int b) {
        return a + b;
    }

}
