package net.agilob.common;

import org.apache.commons.lang3.StringUtils;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class StringContains {

    private static final String STRING_1 = "dsadasdas__dsadsadadas";

    @Benchmark
    public boolean indexOf() {
        return STRING_1.indexOf("__") != -1;
    }

    @Benchmark
    public boolean stringContains() {
        return STRING_1.contains("__");
    }

    @Benchmark
    public boolean apacheUtils() {
        return StringUtils.contains(STRING_1, "__");
    }
}
