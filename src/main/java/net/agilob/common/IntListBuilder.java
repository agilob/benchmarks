package net.agilob.common;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class IntListBuilder {

    private static final int MIN = 0;

    private static final int MAX = 1_000_000;

    @Benchmark
    public List<Integer> buildUsingStream() {
        return IntStream.range(MIN, MAX).boxed().collect(Collectors.toList());
    }

    @Benchmark
    public List<Integer> buildUsingParallelStream() {
        return IntStream.range(MIN, MAX).parallel().boxed().collect(Collectors.toList());
    }

    @Benchmark
    public List<Integer> buildUsingForUnknownSize() {
        List<Integer> ints = new ArrayList<>();
        for (int i = MIN; i < MAX; i++) {
            ints.add(i);
        }
        return ints;
    }


    @Benchmark
    public List<Integer> buildUsingForEachKnownSize() {
        List<Integer> ints = new ArrayList<>(MAX);
        for (int i = MIN; i < MAX; i++) {
            ints.add(i);
        }
        return ints;
    }
}
