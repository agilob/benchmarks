package net.agilob.common;

import java.util.concurrent.atomic.AtomicInteger;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
@State(value = Scope.Benchmark)
public class Increment {

    @Benchmark
    public int atomicIncrement() {
        AtomicInteger ai = new AtomicInteger(0);
        for (int i = 0; i < 100; i++) {
            ai.addAndGet(1);
        }
        return ai.get();
    }

    private volatile int in = 0;

    @Benchmark
    public int volatileIntIncrementPlusPlus() {
        for (int i = 0; i < 100; i++) {
            in++;
        }
        return in;
    }

    @Benchmark
    public int intIncrementPlusPlus() {
        int in = 0;
        for (int i = 0; i < 100; i++) {
            in++;
        }
        return in;
    }

    @Benchmark
    public int intIncrementPlusOne() {
        int in = 0;
        for (int i = 0; i < 100; i++) {
            in = in + 1;
        }
        return in;
    }

    @Benchmark
    // this one hits integer cache subsystem
    public int integerIncrement0to100() {
        Integer integ = new Integer(0);
        for (int i = 0; i < 100; i++) {
            integ = integ + 1;
        }
        return integ;
    }

    @Benchmark
    // this one should NOT hit integer cache subsystem
    public int integerIncrement5000to5100() {
        Integer integ = new Integer(5000);
        for (int i = 5000; i < 5100; i++) {
            integ = integ + 1;
        }
        return integ;
    }

}
