package net.agilob.common;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Threads;
import org.openjdk.jmh.annotations.Warmup;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class RandomLongGenerator {

    @Benchmark
    public long useThreadLocalRandom() {
        return ThreadLocalRandom.current().nextLong();
    }

    @Benchmark
    public long useNewRandom() {
        return new Random().nextLong();
    }

    @Benchmark
    public long useSingleRandom() {
        Random random = new Random();
        return random.nextLong();
    }

    @Benchmark
    public long useSecureRandom() {
        SecureRandom random = new SecureRandom();
        return random.nextLong();
    }

    @Benchmark
    @Threads(4)
    public long useSingleRandomMultiThread() {
        Random random = new Random();
        return random.nextLong();
    }

    @Benchmark
    @Threads(4)
    public long useSecureRandomMultiThread() {
        SecureRandom random = new SecureRandom();
        return random.nextLong();
    }

    @Benchmark
    @Threads(4)
    public long useCachedRandomMultiThread() {
        ThreadLocal<Random> cached = ThreadLocal.withInitial(Random::new);
        return cached.get().nextLong();
    }

    @Benchmark
    @Threads(4)
    public long useNewRandomMultiThread() {
        return new Random().nextLong();
    }

    @Benchmark
    @Threads(4)
    public long useThreadLocalRandomMultiThread() {
        return ThreadLocalRandom.current().nextLong();
    }
}
