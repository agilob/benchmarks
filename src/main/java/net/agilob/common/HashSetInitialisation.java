package net.agilob.common;

import com.google.common.collect.Sets;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

@Fork(value = 2)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class HashSetInitialisation {

    @Benchmark
    public void inlineHashSetInit(Blackhole blackhole) {
        final Set<String> hashSet = new HashSet<String>() {{
            this.add("1");
            this.add("2");
            this.add("3");
            this.add("4");
            this.add("5");
            this.add("6");
            this.add("7");
            this.add("8");
            this.add("9");
            this.add("0");
        }};
        blackhole.consume(hashSet);
    }

    @Benchmark
    public void hashSetInitFromArrays(Blackhole blackhole) {
        final Set<String> hashSet = new HashSet<String>(
                Arrays.asList("0","1","2","3","4","5","6","7","8","9")
        );

        blackhole.consume(hashSet);
    }

    @Benchmark
    public void guavaSetsNewHashSet(Blackhole blackhole) {
        final Set<String> hashSet = Sets.newHashSet("0","1","2","3","4","5","6","7","8","9");
        blackhole.consume(hashSet);
    }

//    @Benchmark
//    public void setsOf(Blackhole blackhole) {
//        final Set<String> hashSet = Set.of("1", "2", "3", "4", "5", "6", "7", "8", "9");
//        blackhole.consume(hashSet);
//    }

}
