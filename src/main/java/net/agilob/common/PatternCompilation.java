package net.agilob.common;

import java.util.regex.Pattern;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class PatternCompilation {

    private static final String PATTERN = "^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$";

    private static final Pattern COMPILED_PATTERN = Pattern.compile(PATTERN);

    private static final String ROMAN_NUMBER = "MMXVIII";

    @Benchmark
    public Boolean compiledPattern() {
        return COMPILED_PATTERN.matcher(ROMAN_NUMBER).matches();
    }

    @Benchmark
    public Boolean stringMatcher() {
        return ROMAN_NUMBER.matches(PATTERN);
    }
}
