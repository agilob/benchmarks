package net.agilob.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
@State(value = Scope.Benchmark)
public class ListBenchmark {

    private List<String> emptyList = new ArrayList<>();

    private List<String> notEmptyList = Arrays.asList("dsadas", "dasd", "dsada");

    private Set<String> emptySet = new HashSet<>();

    private Set<String> notEmptySet = new HashSet<>(notEmptyList);

    private String[] arrayString = new String[10];

    @Benchmark
    public boolean isEmptyOnEmptyArray() {
        return arrayString.length != 0;
    }

    @Benchmark
    public boolean isEmptyOnEmptyList() {
        return !emptyList.isEmpty();
    }

    @Benchmark
    public boolean isSizeGreaterThanZeroOnEmptyList() {
        return emptyList.size() > 0;
    }

    @Benchmark
    public boolean isEmptyOnNotEmptyList() {
        return !notEmptyList.isEmpty();
    }

    @Benchmark
    public boolean isSizeGreaterThanZeroOnNotEmptyList() {
        return notEmptyList.size() > 0;
    }

    @Benchmark
    public boolean isEmptyOnEmptySet() {
        return !emptySet.isEmpty();
    }

    @Benchmark
    public boolean isSizeGreaterThanZeroOnEmptySet() {
        return emptySet.size() > 0;
    }

    @Benchmark
    public boolean isEmptyOnNotEmptySet() {
        return !notEmptySet.isEmpty();
    }

    @Benchmark
    public boolean isSizeGreaterThanZeroOnNotEmptySet() {
        return notEmptySet.size() > 0;
    }
}
