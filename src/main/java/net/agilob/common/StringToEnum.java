package net.agilob.common;

import net.agilob.model.IamEnum;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

@Fork(value = 2)
@Warmup(iterations = 1)
@Measurement(iterations = 5)
public class StringToEnum {

    private static final List<IamEnum> I_AM_ENUM = Arrays.asList(
            IamEnum.ENUM_0, IamEnum.ENUM_1, IamEnum.ENUM_2, IamEnum.ENUM_3,
            IamEnum.ENUM_4,
            IamEnum.ENUM_5, IamEnum.ENUM_6, IamEnum.ENUM_7, IamEnum.ENUM_8);

    // part of fromConcurrentMap method
    private static final Map<String, IamEnum> ENUM_MAP = new ConcurrentHashMap<>();
    static {
        EnumSet.allOf(IamEnum.class).forEach(en ->
                ENUM_MAP.put(en.toString(), en));
    }

    @Benchmark
    public void switchCase(Blackhole blackhole) {
        final String iamEnumString = randomEnum().toString();
        IamEnum iamEnum = null;
        switch (iamEnumString) {
            case "ENUM_0":
                iamEnum = IamEnum.ENUM_0;
                break;
            case "ENUM_1":
                iamEnum = IamEnum.ENUM_1;
                break;
            case "ENUM_2":
                iamEnum = IamEnum.ENUM_2;
                break;
            case "ENUM_3":
                iamEnum = IamEnum.ENUM_3;
                break;
            case "ENUM_4":
                iamEnum = IamEnum.ENUM_4;
                break;
            case "ENUM_5":
                iamEnum = IamEnum.ENUM_5;
                break;
            case "ENUM_6":
                iamEnum = IamEnum.ENUM_6;
                break;
            case "ENUM_7":
                iamEnum = IamEnum.ENUM_7;
                break;
            case "ENUM_8":
                iamEnum = IamEnum.ENUM_8;
                break;
        }
        blackhole.consume(iamEnum);
    }

    @Benchmark
    public void valueOf(Blackhole blackhole) {
        final String iamEnumString = randomEnum().toString();
        IamEnum iamEnum = IamEnum.valueOf(iamEnumString);
        blackhole.consume(iamEnum);
    }

    @Benchmark
    public void fromConcurrentMap(Blackhole blackhole) {
        final String iamEnumString = randomEnum().toString();
        IamEnum iamEnum = ENUM_MAP.get(iamEnumString);
        blackhole.consume(iamEnum);
    }

    @Benchmark
    public void forLoopWithIf(Blackhole blackhole) {
        final String iamEnumString = randomEnum().toString();
        IamEnum iamEnum = null;
        for(IamEnum aie : IamEnum.values()) {
            if(iamEnumString.equals(aie.toString())) {
                iamEnum = aie;
                break;
            }
        }
        blackhole.consume(iamEnum);
    }

    private IamEnum randomEnum() {
        return I_AM_ENUM.get(ThreadLocalRandom.current().nextInt(0, I_AM_ENUM.size()));
    }
}
