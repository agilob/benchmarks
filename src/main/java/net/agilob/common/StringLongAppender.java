package net.agilob.common;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class StringLongAppender {

    static final Long nextLong = ThreadLocalRandom.current().nextLong();

    static final String Uid = UUID.randomUUID().toString();

    @Benchmark
    public String stringValueOf() {
        return String.valueOf(nextLong) + "_" + ThreadLocalRandom.current().nextInt() + "_" + Uid;
    }

    @Benchmark
    public String stringPlus() {
        return Uid + "_" + nextLong + "_" + ThreadLocalRandom.current().nextInt();
    }
}
