package net.agilob.common;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class Log4jBenchmark {

    private static final Logger LOGGER = LoggerFactory.getLogger(Log4jBenchmark.class);

    private static final String SUBSTITUTE = "test";

    @Benchmark
    public void stringLoggerError() {
        LOGGER.error(SUBSTITUTE);
    }

    @Benchmark
    public void stringLoggerTrace() {
        LOGGER.trace(SUBSTITUTE);
    }

    @Benchmark
    public void stringSystemOutPrintln() {
        System.out.println(SUBSTITUTE);
    }

    /* --------------------- */

    @Benchmark
    public void substitutedLoggerError() {
        LOGGER.error("Test: {}", SUBSTITUTE);
    }

    @Benchmark
    public void substitutedLoggerTrace() {
        LOGGER.trace("Test: {}", SUBSTITUTE);
    }

    @Benchmark
    public void substitutedSystemOutPrintlnConcat() {
        System.out.println("Test: " + SUBSTITUTE);
    }

    @Benchmark
    public void substitutedSystemOutPrintlnUsingStringFormat() {
        System.out.println(String.format("Test: %s", SUBSTITUTE));
    }

    /* --------------------- */

    @Benchmark
    public void concatLoggerError() {
        LOGGER.error("Test: " + SUBSTITUTE);
    }

    @Benchmark
    public void concatLoggerTrace() {
        LOGGER.trace("Test: " + SUBSTITUTE);
    }

    @Benchmark
    public void concatSystemOutPrintln() {
        System.out.println("Test: " + SUBSTITUTE);
    }

    /* --------------------- */

    @Benchmark
    public void exceptionMessageLoggerError() {
        try {
            throwingRuntimeException();
        } catch (RuntimeException re) {
            LOGGER.error("Exception: {}", re.getMessage());
        }
    }

    @Benchmark
    public void exceptionMessageLoggerTrace() {
        try {
            throwingRuntimeException();
        } catch (RuntimeException re) {
            LOGGER.trace("Exception: {}", re.getMessage());
        }
    }

    @Benchmark
    public void exceptionMessageSystemErrPrintln() {
        try {
            throwingRuntimeException();
        } catch (RuntimeException re) {
            System.err.println(re.getMessage());
        }
    }

    /* --------------------- */

    @Benchmark
    public void exceptionStacktraceLoggerError() {
        try {
            throwingRuntimeException();
        } catch (RuntimeException re) {
            LOGGER.error("Exception: ", re);
        }
    }

    @Benchmark
    public void exceptionStacktraceLoggerTrace() {
        try {
            throwingRuntimeException();
        } catch (RuntimeException re) {
            LOGGER.trace("Exception: ", re);
        }
    }

    @Benchmark
    public void exceptionStacktraceSystemErrPrintln() {
        try {
            throwingRuntimeException();
        } catch (RuntimeException re) {
            re.printStackTrace();
        }
    }

    private void throwingRuntimeException() {
        throw new RuntimeException("I AM EXCEPTION");
    }
}
