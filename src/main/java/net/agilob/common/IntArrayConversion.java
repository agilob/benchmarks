package net.agilob.common;

import java.util.Arrays;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class IntArrayConversion {

    @Benchmark
    public int[][] convertUsingStream() {
        String stringArray[][] = { { "21", "22", "23" }, { "31", "32" }, { "1", "2", "3" } };
        int[][] intArray = new int[stringArray.length][];
        for (int i = 0; i < stringArray.length; i++) {
            intArray[i] = Arrays.stream(stringArray[i]).mapToInt(Integer::parseInt).toArray();
        }
        return intArray;
    }

    @Benchmark
    public int[][] convertUsingLoop() {
        String stringArray[][] = { { "21", "22", "23" }, { "31", "32" }, { "1", "2", "3" } };

        int[][] arrayInt = new int[stringArray.length][];
        for (int i = 0; i < stringArray.length; i++) {
            int ints[] = new int[stringArray[i].length];
            for (int j = 0; j < stringArray[i].length; j++) {
                ints[j] = Integer.parseInt((stringArray[i][j]));
                arrayInt[i] = ints;
            }
        }
        return arrayInt;
    }
}
