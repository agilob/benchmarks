package net.agilob.common;

import org.openjdk.jmh.annotations.*;

import java.util.UUID;

@State(Scope.Thread)
@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 15)
public class StringTextAppender {
    private String Uid;

    @Setup
    public void setup() {
        Uid = UUID.randomUUID().toString();
    }

    @Benchmark
    public String stringPlusString() {
        return Uid + UUID.randomUUID();
    }

    @Benchmark
    public String stringConcatString() {
        return Uid.concat(UUID.randomUUID().toString());
    }

    @Benchmark
    public String stringBuilder() {
        return new StringBuilder()
                .append(Uid)
                .append(UUID.randomUUID())
                .toString();
    }

    @Benchmark
    public String stringBuilderWithInitialCapacity() {
        return new StringBuilder(Uid.length())
                .append(Uid)
                .append(UUID.randomUUID())
                .toString();
    }

    @Benchmark
    public String stringBuffer() {
        return new StringBuffer()
                .append(Uid)
                .append(UUID.randomUUID())
                .toString();
    }

    @Benchmark
    public String stringBufferWithInitialCapacity() {
        return new StringBuffer(Uid.length())
                .append(Uid)
                .append(UUID.randomUUID())
                .toString();
    }
}
