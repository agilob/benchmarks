package net.agilob.common;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@State(Scope.Thread)
@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class ForLooping {

    @Param({"10", "1000", "10000", "100000"})
    public int iteration;

    private List<Integer> theList;

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(ForLooping.class.getSimpleName())
                .build();

        new Runner(opt).run();
    }

    @Setup(Level.Trial)
    public void setup() {
        theList = new ArrayList<>(iteration);
        for(int i = 0; i < iteration; i++) {
            theList.add(ThreadLocalRandom.current().nextInt());
        }
    }

    @Benchmark
    public void forIntI(Blackhole blackhole) {
        for(int i = 0; i < theList.size(); i++) {
            blackhole.consume(i);
        }
    }

    @Benchmark
    public void forEachIterator(Blackhole blackhole) {
        for (Integer integer : theList) {
            blackhole.consume(integer);
        }
    }

    @Benchmark
    public void forEachIteratorInTryCatch(Blackhole blackhole) {
        try {
            for (Integer integer : theList) {
                blackhole.consume(integer);
            }
        } catch (Exception e) {

        }
    }

    @TearDown
    public void tearDown() {
        theList = null;
    }
}
