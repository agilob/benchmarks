package net.agilob.common;

import net.agilob.model.IamEnum;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Fork(value = 2)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class FindingEnum {

    private static final EnumSet<IamEnum> ALL_ENUMS_SET = EnumSet.allOf(IamEnum.class);
    private static final HashSet<IamEnum> ALL_ENUMS_HASHSET = new HashSet<>(ALL_ENUMS_SET);
    private static final List<IamEnum> ALL_ENUMS_ARRAYLIST = new ArrayList<>(ALL_ENUMS_SET);

    @Benchmark
    public void findEnumInArrayList(Blackhole blackhole) {
        IamEnum findMe = randomEnum();
        blackhole.consume(ALL_ENUMS_ARRAYLIST.contains(findMe));
    }

    @Benchmark
    public void findEnumInEnumSet(Blackhole blackhole) {
        IamEnum findMe = randomEnum();
        blackhole.consume(ALL_ENUMS_SET.contains(findMe));
    }

    @Benchmark
    public void findEnumInHashSet(Blackhole blackhole) {
        IamEnum findMe = randomEnum();
        blackhole.consume(ALL_ENUMS_HASHSET.contains(findMe));
    }

    @Benchmark
    public void findEnumByStreamFindAny(Blackhole blackhole) {
        IamEnum findMe = randomEnum();
        blackhole.consume(ALL_ENUMS_HASHSET.stream().filter(e -> e == findMe).findAny().get());
    }

    @Benchmark
    public void findEnumByStreamFindFirst(Blackhole blackhole) {
        IamEnum findMe = randomEnum();
        blackhole.consume(ALL_ENUMS_HASHSET.stream().filter(e -> e == findMe).findFirst().get());
    }

    @Benchmark
    public void findEnumByParallelStreamFindAny(Blackhole blackhole) {
        IamEnum findMe = randomEnum();
        blackhole.consume(ALL_ENUMS_HASHSET.parallelStream().filter(e -> e == findMe).findAny().get());
    }

    @Benchmark
    public void findEnumByParallelStreamFindFirst(Blackhole blackhole) {
        IamEnum findMe = randomEnum();
        blackhole.consume(ALL_ENUMS_HASHSET.parallelStream().filter(e -> e == findMe).findFirst().get());
    }

    private IamEnum randomEnum() {
        return ALL_ENUMS_ARRAYLIST.get(ThreadLocalRandom.current().nextInt(0, ALL_ENUMS_ARRAYLIST.size()));
    }
}
