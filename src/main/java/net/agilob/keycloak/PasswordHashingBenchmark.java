/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.agilob.keycloak;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.spec.KeySpec;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Fork(value = 3, jvmArgsAppend = {"-Xmx2000m"})
@Warmup(iterations = 1)
@Measurement(iterations = 5)
public class PasswordHashingBenchmark {

    @Param({"password",
            "p([n:7_FSr0Su:`3g_\"1:\\FC8z-Q4qEm^gOwTd^HVZS[5P9$08},IZ1>}&=qLQ}#O_PzQd]`aPe_JL~by`4>#?XM?bTn]kPo?GOIy4C2Tv20I<]O|_\"z:b(]sBnVO@D]>7wkNH)%[|s3.`vzNWayO~\\xD;O.bNA6$5]*iT9y`0<a`x$\"v'e~2|o,{'G0TML-#4ZV)3\"`cGHxtdmK6/RtP>+phPN\"JjIybFbBP&.pusulTN|iXh:ynpW$qpO=m:|Le'^\"TSXsUTg:D!Z->xf/`.;.|24$Z5x3OCmmu{Jt-}+<p#{)S<\"\\7e-+`hi4*&62;_q+H4xj<)?v+::\"E`@^#s{+~e`9XG/Nm?7+unDY$CoH(X*&qP'NE02K-{y2}6{/8?\\xKYiFK(CUn_`c9bvy<bQXr&&!R,'XBybe2i\"9NY;$U'rAl15h8}h7,m'yvr8o{FMIFgYqj6`{[V+W#\\1={:I_%TJ!Ooy67:1OZ60Uyr10#A'3>yU!]dDBUXz$+_[r'Lm}spFfs&(2R12]e-Z*&^b]{kEY&iQtX0_{O`\"t-JiXBZ{zjDFQA@Da*^76$2Mgf4d*[CiTqm8\"oH;TdZA.}&qYr(a7a0;:\"Ist]ES|v(HXq8Yn-s8V7\\2qhgR6*|O)2}![#Lc@FF5%3(#TC=p(6va4NN"})
    public String password;

    @Param({"Hy9X2DNQNOVYOictvV0wrA==",
            "rqdzpZGc2XqFgjMeCRVZLg=="})
    public String salt;

    @Param({"1", "1000", "15000", "27500"})
    public int iteration;

    @Benchmark
    public String testPbkdf2Sha1() throws Exception {
        return hasPassword(password, salt.getBytes(), "PBKDF2WithHmacSHA1", iteration);
    }

    private String hasPassword(String password, byte[] salt, String algorithm, int iterations) throws Exception {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, 512);
        byte[] key = SecretKeyFactory.getInstance(algorithm).generateSecret(spec).getEncoded();
        return Base64.encodeBytes(key);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(PasswordHashingBenchmark.class.getSimpleName())
                .build();

        new Runner(opt).run();
    }

}
