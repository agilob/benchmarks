package net.agilob.keycloak;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.lang.management.ManagementFactory;
import java.security.spec.KeySpec;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Fork(value = 3)
@Warmup(iterations = 10, time = 500, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 15, time = 1, timeUnit = TimeUnit.SECONDS)
public class PasswordHashingPerGC {

    @Param({"p([nO)2}![#Lc@FF5%3(#TC=p(6va4NN"})
    public String password;

    @Param({"Hy9X2DNQNOVYOictvV0wrA=="})
    public String salt;

    @Param({"27500"})
    public int iterations;

    @Benchmark
    @Fork(jvmArgsAppend = {"-XX:+UseZGC", "-Xmx2000m"})
    public String testPbkdf2Sha1ZGC() throws Exception {
        return hasPassword(password, salt.getBytes(), "PBKDF2WithHmacSHA1", iterations);
    }

    @Benchmark
    @Fork(jvmArgsPrepend = {"-XX:+UseShenandoahGC", "-Xmx2000m"})
    public String testPbkdf2Sha1ShenandoahGC() throws Exception {
        return hasPassword(password, salt.getBytes(), "PBKDF2WithHmacSHA1", iterations);
    }

    @Benchmark
    @Fork(jvmArgsAppend = {"-XX:+UseG1GC", "-Xmx2000m"})
    public String testPbkdf2Sha1G1() throws Exception {
        return hasPassword(password, salt.getBytes(), "PBKDF2WithHmacSHA1", iterations);
    }

    @Benchmark
    @Fork(jvmArgsAppend = {"-XX:+UseParallelGC", "-Xmx2000m"})
    public String testPbkdf2Sha1ParallelGC() throws Exception {
        return hasPassword(password, salt.getBytes(), "PBKDF2WithHmacSHA1", iterations);
    }

    private String hasPassword(String password, byte[] salt, String algorithm, int iterations) throws Exception {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, 512);
        byte[] key = SecretKeyFactory.getInstance(algorithm).generateSecret(spec).getEncoded();
        return Base64.encodeBytes(key);
    }
    
    @TearDown
    public void tearDown() {
        for (final var bean : ManagementFactory.getGarbageCollectorMXBeans()) {
            System.out.println(bean.getName());
            System.out.println("   Count: " + bean.getCollectionCount());
            System.out.println("   Total Time: " + bean.getCollectionTime() + "ms");
            System.out.println("   Average Time: " + (bean.getCollectionTime() / (double)bean.getCollectionCount()) + "ms");
        }
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(PasswordHashingPerGC.class.getSimpleName())
                .build();

        new Runner(opt).run();
    }

}
