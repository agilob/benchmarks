package net.agilob.keycloak;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.spec.KeySpec;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Fork(value = 3)
@Warmup(iterations = 1)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
public class PasswordHashingIterations {

    @Param({"password"})
    public String password;

    @Param({"Hy9X2DNQNOVYOictvV0wrA=="})
    public String salt;

    @Param({"1", "10", "1000", "10000", "15000", "20000", "27500", "50000", "100000"})
    public int iteration;

    @Benchmark
    public String testPbkdf2Sha1() throws Exception {
        return hasPassword(password, salt.getBytes(), "PBKDF2WithHmacSHA1", iteration);
    }

    private String hasPassword(String password, byte[] salt, String algorithm, int iterations) throws Exception {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, 512);
        byte[] key = SecretKeyFactory.getInstance(algorithm).generateSecret(spec).getEncoded();
        return Base64.encodeBytes(key);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(PasswordHashingIterations.class.getSimpleName())
                .build();

        new Runner(opt).run();
    }
}
