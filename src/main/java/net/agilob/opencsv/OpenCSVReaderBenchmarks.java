package net.agilob.opencsv;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Fork(value = 5)
@Warmup(iterations = 1)
@Measurement(iterations = 10, time = 2, timeUnit = TimeUnit.SECONDS)
public class OpenCSVReaderBenchmarks {

    @Param({"src/main/resources/1_000_000_users.csv", "src/main/resources/10_users.csv"})
    private String fileSize;

    private OpenCSVReader<CSVUser> openCSVReader;

    @Setup
    public void setup() {
        openCSVReader = new OpenCSVReader<>();
    }

    @Benchmark
    public void getLastUserIterator(Blackhole blackhole) throws IOException {
        var tIterator = openCSVReader.openIteratorReader(fileSize, CSVUser.class);

        CSVUser lastInstance = null;
        while(tIterator.hasNext()) {
            lastInstance = tIterator.next();
        }
        blackhole.consume(lastInstance);
    }

    @Benchmark
    public void getLastUserStream(Blackhole blackhole) throws IOException {
        var csvUserStream = openCSVReader.openStreamReader(fileSize, CSVUser.class);
        final var lastInstance = csvUserStream.reduce((first, second) -> second).orElseThrow(() -> new RuntimeException("No last element"));
        blackhole.consume(lastInstance);
    }

    @Benchmark
    public void getLastUserParallelStream(Blackhole blackhole) throws IOException {
        var csvUserParallelStream = openCSVReader.openStreamReader(fileSize, CSVUser.class);
        final var lastInstance = csvUserParallelStream.parallel().reduce((first, second) -> second).orElseThrow(() -> new RuntimeException("No last element"));
        blackhole.consume(lastInstance);
    }

    @TearDown
    public void closeFile() {
        openCSVReader.closeStreamReader();
    }
}
