package net.agilob.opencsv;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.stream.Stream;

public class OpenCSVReader<T> {

    private Reader reader;

    public Iterator<T> openIteratorReader(final String filename, final Class<T> clazz) throws IOException {
        reader = new FileReader(Path.of(filename).toString());

        final CsvToBean<T> csvReader = new CsvToBeanBuilder<T>(reader)
                .withType(clazz)
                .withSeparator(',')
                .withIgnoreEmptyLine(true)
                .withSkipLines(1) //skip header
                .build();

        return csvReader.iterator();
    }

    public Stream<T> openStreamReader(final String filename, final Class<T> clazz) throws IOException {
        reader = new FileReader(Path.of(filename).toString());

        final CsvToBean<T> csvReader = new CsvToBeanBuilder<T>(reader)
                .withType(clazz)
                .withSeparator(',')
                .withIgnoreEmptyLine(true)
                .withSkipLines(1) //skip header
                .build();

        return csvReader.stream();
    }

    public void closeStreamReader() {
        try {
            if(reader != null) {
                reader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
