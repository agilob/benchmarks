package net.agilob.opencsv;

import com.opencsv.bean.CsvBindByPosition;

public class CSVUser {

    @CsvBindByPosition(position = 0)
    private String userId;

    @CsvBindByPosition(position = 1)
    private String username;

    @CsvBindByPosition(position = 2)
    private String deviceId;

    @CsvBindByPosition(position = 3)
    private String biometricKeyAlias;

    @CsvBindByPosition(position = 4)
    private String passcodeKeyAlias;

    @CsvBindByPosition(position = 5)
    private String confirmationId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getBiometricKeyAlias() {
        return biometricKeyAlias;
    }

    public void setBiometricKeyAlias(String biometricKeyAlias) {
        this.biometricKeyAlias = biometricKeyAlias;
    }

    public String getPasscodeKeyAlias() {
        return passcodeKeyAlias;
    }

    public void setPasscodeKeyAlias(String passcodeKeyAlias) {
        this.passcodeKeyAlias = passcodeKeyAlias;
    }

    public String getConfirmationId() {
        return confirmationId;
    }

    public void setConfirmationId(String confirmationId) {
        this.confirmationId = confirmationId;
    }
}
