package net.agilob.backbase;

import net.agilob.model.IamEnum;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.EnumSet;

@Fork(value = 1, jvmArgs = "-Xmx2048m")
@Warmup(iterations = 2)
@State(Scope.Benchmark)
@Measurement(iterations = 10, time = 5)
public class EnumIteration {

    @Benchmark
    public void values(Blackhole bh) {
        for (IamEnum it : IamEnum.values()) {
            bh.consume(it.ordinal());
        }
    }

    @Benchmark
    public void cachedValues(Blackhole bh) {
        for (IamEnum it : IamEnum.MY_VALUES) {
            bh.consume(it.ordinal());
        }
    }

    @Benchmark
    public void enumSetValues(Blackhole bh) {
        for (IamEnum it : EnumSet.allOf(IamEnum.class)) {
            bh.consume(it.ordinal());
        }
    }

    @Benchmark
    public void enumSetCachedValues(Blackhole bh) {
        for (IamEnum it : IamEnum.SET_VALUES) {
            bh.consume(it.ordinal());
        }
    }
}
