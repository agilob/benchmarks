package net.agilob.backbase;

import org.openjdk.jmh.annotations.*;

@Fork(value = 3, jvmArgs = {"-XX:+PrintCompilation", "-XX:+UnlockDiagnosticVMOptions", "-XX:+PrintInlining"})
@Warmup(iterations = 2)
@State(Scope.Benchmark)
@Measurement(iterations = 10)
public class EscapeAnalysis {

    @Benchmark
    public long sumNoEscape() {
        long sum = 0L;
        for(int i = 0; i < 100_000; i++) {
            MyEscape myEscape = new MyEscape(i);
            sum += myEscape.getI();
        }
        return sum;
    }

    @Benchmark
    public long sumWithEscape() {
        long sum = 0L;
        for(int i = 0; i < 100_000; i++) {
            MyEscape myEscape = new MyEscape(i);
            sum += escapeHere(myEscape);
        }
        return sum;
    }

    @CompilerControl(CompilerControl.Mode.DONT_INLINE)
    private long escapeHere(MyEscape i) {
        return i.getI();
    }

    public static class MyEscape {
        private long i;

        public MyEscape(long i) {
            this.i = i;
        }

        public long getI() {
            return i;
        }
    }

}
