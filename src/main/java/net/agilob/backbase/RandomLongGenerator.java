package net.agilob.backbase;

import org.openjdk.jmh.annotations.*;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@Fork(value = 3)
@Warmup(iterations = 5)
@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Measurement(iterations = 5, time = 1)
public class RandomLongGenerator {

    @Benchmark
    public long useThreadLocalRandom() {
        return ThreadLocalRandom.current().nextLong();
    }

    @Benchmark
    public long useNewRandom() {
        return new Random().nextLong();
    }

    @Benchmark
    public long useSecureRandom() {
        SecureRandom random = new SecureRandom();
        return random.nextLong();
    }

}
