package net.agilob.backbase;

import java.util.Objects;

public class HashMapCompositionPair {

    private final String s1;
    private final String s2;

    public HashMapCompositionPair(String s1, String s2) {
        this.s1 = s1;
        this.s2 = s2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HashMapCompositionPair that = (HashMapCompositionPair) o;
        return Objects.equals(s1, that.s1) && Objects.equals(s2, that.s2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(s1, s2);
    }
}
