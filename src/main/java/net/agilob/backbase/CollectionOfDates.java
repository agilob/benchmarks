package net.agilob.backbase;

import org.openjdk.jmh.annotations.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Fork(value = 3)
@Warmup(iterations = 5)
@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Measurement(iterations = 5, time = 10)
public class CollectionOfDates {

    @Param({"60", "356", "1000", "10000", "100000", "1000000"})
    int capacity;

    @Benchmark
    public List<LocalDate> listUsingFor() {
        List<LocalDate> dates = new ArrayList<>(capacity);
        for(int i = 0; i < capacity; i++) {
            dates.add(LocalDate.now().plusDays(i));
        }
        return dates;
    }

    @Benchmark
    public List<LocalDate> listUsingStream() {
        return IntStream.range(0, capacity)
                .mapToObj((i) -> LocalDate.now().plusDays(i))
                .collect(Collectors.toList());
    }

    @Benchmark
    public List<LocalDate> listUsingStreamParallel() {
        return IntStream.range(0, capacity)
                .parallel()
                .mapToObj((i) -> LocalDate.now().plusDays(i))
                .collect(Collectors.toList());
    }

    @Benchmark
    @Fork(jvmArgsAppend = "-Djava.util.concurrent.ForkJoinPool.common.parallelism=2")
    public List<LocalDate> listUsingStreamParallelBut2() {
        return IntStream.range(0, capacity)
                .parallel()
                .mapToObj((i) -> LocalDate.now().plusDays(i))
                .collect(Collectors.toList());
    }

}
