package net.agilob.backbase;

import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Fork(value = 3)
@Warmup(iterations = 5)
@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Measurement(iterations = 5, time = 1)
public class RemoveIf {

    private static final int size = 1000;

    private List<Integer> randoms;

    @Setup
    public final void setup() {
        randoms = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            randoms.add(ThreadLocalRandom.current().nextInt());
        }
    }

    @Benchmark
    public List<Integer> predicateRemoveIf() {
        randoms.removeIf(l -> l < 100_000);
        return randoms;
    }

    @Benchmark
    public List<Integer> forLoopRemoveIf() {
        for (Iterator<Integer> iterator = randoms.iterator(); iterator.hasNext();) {
            if(iterator.next() < 100_000) {
                iterator.remove();
            }
        }
        return randoms;
    }

    @Benchmark
    public List<Integer> streamFilter() {
        return randoms.stream()
                .filter((i) -> i >= 100_000)
                .collect(Collectors.toList());
    }
}
