package net.agilob.backbase;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.ThreadLocalRandom;

@Fork(value = 3)
@Warmup(iterations = 5)
@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Measurement(iterations = 5, time = 1)
public class TryCatchNumberFormatting {

    private String I_AM_INTEGER;

    @Setup(Level.Invocation)
    public void setup() {
        I_AM_INTEGER = String.valueOf(ThreadLocalRandom.current().nextInt());
    }

    @Benchmark
    public Integer justParsing() {
        return Integer.valueOf(I_AM_INTEGER);
    }

    @Benchmark
    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    public Integer justParsingCompilerControlled() {
        return Integer.valueOf(I_AM_INTEGER);
    }

    @Benchmark
    public Integer justParsingWithTryCatch() {
        try {
            return Integer.valueOf(I_AM_INTEGER);
        } catch (NumberFormatException nfe) {

        }
        return Integer.valueOf(0);
    }

    @Benchmark
    public Integer justParsingWithTryCatchToValue() {
        Integer integer = 0;
        try {
            integer = Integer.valueOf(I_AM_INTEGER);
        } catch (NumberFormatException nfe) {

        }
        return integer;
    }

}
