package net.agilob.backbase;

import org.openjdk.jmh.annotations.*;

import java.util.HashMap;
import java.util.Map;

@Fork(value = 3)
@Warmup(iterations = 5)
@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Measurement(iterations = 5, time = 1)
public class HashMapCapacity {

    @Param({"10", "14", "129"})
    int keys;

    @Param({"10", "16", "256"})
    int capacity;

    @Benchmark
    public Map<Integer, Integer> loadHashMap() {
        Map<Integer, Integer> map = new HashMap<>(capacity);
        for (int i = 0; i < keys; ++i) {
            map.put(i, i);
        }
        return map;
    }
}
