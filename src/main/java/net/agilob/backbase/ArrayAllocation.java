package net.agilob.backbase;

import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Fork(value = 3)
@Warmup(iterations = 5)
@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Measurement(iterations = 5, time = 1)
public class ArrayAllocation {

    @Benchmark
    public int arrayAllocation63() {
        int[] a = new int[63];
        a[0] = ThreadLocalRandom.current().nextInt();
        a[1] = ThreadLocalRandom.current().nextInt();

        return a[0] + a[1];
    }

    @Benchmark
    public int arrayAllocation64() {
        int[] a = new int[64];
        a[0] = ThreadLocalRandom.current().nextInt();
        a[1] = ThreadLocalRandom.current().nextInt();

        return a[0] + a[1];
    }

    @Benchmark
    public int arrayAllocation65() {
        int[] a = new int[65];
        a[0] = ThreadLocalRandom.current().nextInt();
        a[1] = ThreadLocalRandom.current().nextInt();

        return a[0] + a[1];
    }

    @Benchmark
    @Fork(jvmArgsAppend = "-XX:EliminateAllocationArraySizeLimit=102")
    public int arrayAllocation77() {
        int[] a = new int[77];
        a[0] = ThreadLocalRandom.current().nextInt();
        a[1] = ThreadLocalRandom.current().nextInt();

        return a[0] + a[1];
    }

    @Benchmark
    public int arrayAllocation80() {
        int[] a = new int[80];
        a[0] = ThreadLocalRandom.current().nextInt();
        a[1] = ThreadLocalRandom.current().nextInt();

        return a[0] + a[1];
    }

    @Benchmark
    @Fork(jvmArgsAppend = "-XX:EliminateAllocationArraySizeLimit=102")
    public int arrayAllocation85() {
        int[] a = new int[85];
        a[0] = ThreadLocalRandom.current().nextInt();
        a[1] = ThreadLocalRandom.current().nextInt();

        return a[0] + a[1];
    }

    @Benchmark
    public int arrayAllocation90() {
        int[] a = new int[90];
        a[0] = ThreadLocalRandom.current().nextInt();
        a[1] = ThreadLocalRandom.current().nextInt();

        return a[0] + a[1];
    }

    @Benchmark
    @Fork(jvmArgsAppend = "-XX:EliminateAllocationArraySizeLimit=102")
    public int arrayAllocation95() {
        int[] a = new int[95];
        a[0] = ThreadLocalRandom.current().nextInt();
        a[1] = ThreadLocalRandom.current().nextInt();

        return a[0] + a[1];
    }

    @Benchmark
    public int listAllocationSize63() {
        List<Integer> list = new ArrayList<>(63);
        list.add(ThreadLocalRandom.current().nextInt());
        list.add(ThreadLocalRandom.current().nextInt());
        return list.get(0) + list.get(1);
    }

    @Benchmark
    public int listAllocationSize64() {
        List<Integer> list = new ArrayList<>(64);
        list.add(ThreadLocalRandom.current().nextInt());
        list.add(ThreadLocalRandom.current().nextInt());
        return list.get(0) + list.get(1);
    }

    @Benchmark
    @Fork(jvmArgsAppend = "-XX:EliminateAllocationArraySizeLimit=102")
    public int listAllocationSize65() {
        List<Integer> list = new ArrayList<>(65);
        list.add(ThreadLocalRandom.current().nextInt());
        list.add(ThreadLocalRandom.current().nextInt());
        return list.get(0) + list.get(1);
    }
}
