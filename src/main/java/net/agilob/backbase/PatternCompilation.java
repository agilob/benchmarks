package net.agilob.backbase;

import org.openjdk.jmh.annotations.*;

import java.util.regex.Pattern;

@Fork(value = 3)
@Warmup(iterations = 5)
@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Measurement(iterations = 5, time = 1)
public class PatternCompilation {

    private static final String PATTERN = "^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$";

    private static final Pattern COMPILED_PATTERN = Pattern.compile(PATTERN);

    private static final String ROMAN_NUMBER = "MMXVIII";

    @Benchmark
    public Boolean compiledPattern() {
        return COMPILED_PATTERN.matcher(ROMAN_NUMBER).matches();
    }

    @Benchmark
    public Boolean stringMatcher() {
        return ROMAN_NUMBER.matches(PATTERN);
    }
}
