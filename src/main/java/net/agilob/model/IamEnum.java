package net.agilob.model;

import java.util.EnumSet;
import java.util.Set;

public enum IamEnum {
    ENUM_0,
    ENUM_1,
    ENUM_2,
    ENUM_3,
    ENUM_4,
    ENUM_5,
    ENUM_6,
    ENUM_7,
    ENUM_8;

    public static final IamEnum[] MY_VALUES = IamEnum.values();

    public static final Set<IamEnum> SET_VALUES = EnumSet.allOf(IamEnum.class);
}
