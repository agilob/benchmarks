package net.agilob.gc;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

@Fork(value = 3)
@Warmup(iterations = 1)
@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Measurement(iterations = 10, time = 5)
public class OverAllocation {

    private static final int HALF_GIGABYTE = 1024 * 1024 * (1024/2);
    private static final int ITERATIONS = 100;

    @Benchmark
    @Fork(jvmArgsAppend = {"-XX:+UseParallelGC", "-Xmx25g"})
    public void generateDataParallel(Blackhole blackhole) {
        // allocate memory 1GB at a time
        for (int i = 0; i < ITERATIONS; i++) {
            final byte[] array = new byte[HALF_GIGABYTE];
            blackhole.consume(array);
        }
    }

    @Benchmark
    @Fork(jvmArgsAppend = {"-XX:+UseShenandoahGC", "-Xmx25g"})
    public void generateDataShenandoah(Blackhole blackhole) {
        // allocate memory 1GB at a time
        for (int i = 0; i < ITERATIONS; i++) {
            final byte[] array = new byte[HALF_GIGABYTE];
            blackhole.consume(array);
        }
    }

    @Benchmark
    @Fork(jvmArgsAppend = {"-XX:+UseG1GC", "-Xmx25g"})
    public void generateDataGC1(Blackhole blackhole) {
        // allocate memory 1GB at a time
        for (int i = 0; i < ITERATIONS; i++) {
            final byte[] array = new byte[HALF_GIGABYTE];
            blackhole.consume(array);
        }
    }

    @Benchmark
    @Fork(jvmArgsAppend = {"-XX:+UseZGC", "-Xmx25g"})
    public void generateDataZGC(Blackhole blackhole) {
        // allocate memory 1GB at a time
        for (int i = 0; i < ITERATIONS; i++) {
            final byte[] array = new byte[HALF_GIGABYTE];
            blackhole.consume(array);
        }
    }

}
