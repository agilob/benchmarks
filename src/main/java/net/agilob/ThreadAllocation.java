package net.agilob;

import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;

public class ThreadAllocation {

    public static void main(String... args) {
        Runnable r1 = () -> {
            System.out.println("Starting r1");
            for(long i  = 0; i < 1_000_0000_000L; i++) {
                var list1 = new ArrayList<String>(1_000);
                list1.add("" + i);
            }
            System.out.println("Finishing r1");
        };
        Runnable r2 = () -> {
            try {
                System.out.println("Starting r2");
                final int HALF_GIGABYTE = 1024 * 1024 * (1024/2) ;
                for (int i = 0; i < 1_000_0000; i++) {
                    final byte[] array = new byte[HALF_GIGABYTE];
                    consume(array);
                }
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            } finally {
                System.out.println("Finishing r2");
            }
        };
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(r1);
        executorService.submit(r2);
        while(!executorService.isShutdown()) {
        }
    }
    public static int consume(byte[] array) {
        return array.length;
    }
}
