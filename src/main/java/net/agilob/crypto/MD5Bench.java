package net.agilob.crypto;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import org.apache.commons.codec.digest.DigestUtils;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.openjdk.jmh.annotations.*;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Fork(value = 3)
@Warmup(iterations = 2)
@Measurement(iterations = 5, time = 1)
public class MD5Bench {

    private static final String password = "ILoveJava";

    @Benchmark
    public String sunSecurity() throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        return new String(digest);
    }

    @Benchmark
    @Fork(jvmArgs ={ "-XX:+UnlockDiagnosticVMOptions", "-XX:+UseMD5Intrinsics"})
    public String sunSecurityWithIntrinsic() throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        return new String(digest);
    }

    @Benchmark
    public String apacheCommons() {
        return DigestUtils.md5Hex(password).toUpperCase();
    }

    @Benchmark
    @Fork(jvmArgs ={ "-XX:+UnlockDiagnosticVMOptions", "-XX:+UseMD5Intrinsics"})
    public String guavaWithIntrinsics() {
        HashFunction hashFunction = Hashing.md5();
        return hashFunction.hashString(password, StandardCharsets.UTF_8).toString();
    }

    @Benchmark
    public String guava() {
        HashFunction hashFunction = Hashing.md5();
        return hashFunction.hashString(password, StandardCharsets.UTF_8).toString();
    }

    @Benchmark
    @Fork(jvmArgs ={ "-XX:+UnlockDiagnosticVMOptions", "-XX:+UseMD5Intrinsics"})
    public String bouncyCastleWithIntrinsics() {
        MD5Digest eTag = new MD5Digest();
        byte[] resBuf = new byte[eTag.getDigestSize()];
        final byte[] bytes = password.getBytes(StandardCharsets.UTF_8);
        eTag.update(bytes,0, bytes.length);
        eTag.doFinal(resBuf,0);
        return new String(resBuf);
    }

    @Benchmark
    public String bouncyCastle() {
        MD5Digest eTag = new MD5Digest();
        byte[] resBuf = new byte[eTag.getDigestSize()];
        final byte[] bytes = password.getBytes(StandardCharsets.UTF_8);
        eTag.update(bytes,0, bytes.length);
        eTag.doFinal(resBuf,0);
        return new String(resBuf);
    }
}
