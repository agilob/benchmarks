package net.agilob.varia;

import org.openjdk.jmh.annotations.*;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class StringToBoolean {

    private final static List<String> randomBoolinicStrings = Arrays.asList("true", "TRUE", "false", "FALSE", null);

    private final static String TRUE = "true";

    private String theString;

    @Setup(Level.Invocation)
    public void setup() {
        theString = randomBoolinicStrings.get(ThreadLocalRandom.current().nextInt(0, randomBoolinicStrings.size()));
    }

    @Benchmark
    public boolean BooleanValueOf() {
        return Boolean.valueOf(theString);
    }

    @Benchmark
    public boolean BooleanParseBoolean() {
        return Boolean.parseBoolean(theString);
    }

    @Benchmark
    public boolean StringEquals() {
        return TRUE.equals(theString);
    }

    @Benchmark
    public boolean StringEqualsIgnoreCase() {
        return TRUE.equalsIgnoreCase(theString);
    }
}
