package net.agilob.varia;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@Fork(value = 3)
@Warmup(iterations = 2)
@Measurement(iterations = 10)
public class SillyGetPercentage {

    private static double percentage = -1;

    @Setup(Level.Invocation)
    public void setup() {
        percentage = ThreadLocalRandom.current().nextDouble(0.0, 0.9);
    }

    @Benchmark
    public void staticGetPercentageAsOnTwitter(Blackhole blackhole) {
        blackhole.consume(getPercentageRounds(percentage));
    }

    @Benchmark
    public void byOpenAI(Blackhole blackhole) {
        String result = "";
        for(int i = 0; i < 10; i++) {
            if(percentage > i * 0.1) {
                result += "o";
            } else {
                result += "O";
            }
        }
        blackhole.consume(result);
    }

    @Benchmark
    public void usingSubstring(Blackhole blackhole) {
        int empty = (int) (10 - percentage * 10);
        final String result = "⬤⬤⬤⬤⬤⬤⬤⬤⬤⬤○○○○○○○○○○"
                .substring(empty, 10 + empty);
        blackhole.consume(result);
    }

    @Benchmark
    public void usingStringArray(Blackhole blackhole) {
        String[] array = {
                "OOOOOOOOOO", "oOOOOOOOOO", "ooOOOOOOOO", "oooOOOOOOO", "ooooOOOOOO",
                "ooooo00000", "ooooooOOOO", "oooooooOOO", "ooooooooOO", "oooooooooO"};
        blackhole.consume(array[(int) Math.round(percentage * 10)]);
    }

    private String[] array = {
            "OOOOOOOOOO", "oOOOOOOOOO", "ooOOOOOOOO", "oooOOOOOOO", "ooooOOOOOO",
            "ooooo00000", "ooooooOOOO", "oooooooOOO", "ooooooooOO", "oooooooooO"};
    @Benchmark
    public void usingExternalStringArray(Blackhole blackhole) {
        blackhole.consume(array[(int) Math.round(percentage * 10)]);
    }

    @Benchmark
    public void usingArrayList(Blackhole blackhole) {
        List<String> array = List.of("OOOOOOOOOO", "oOOOOOOOOO", "ooOOOOOOOO", "oooOOOOOOO", "ooooOOOOOO",
                "ooooo00000", "ooooooOOOO", "oooooooOOO", "ooooooooOO", "oooooooooO");
        blackhole.consume(array.get((int) Math.round(percentage * 10)));
    }

    @Benchmark
    public void usingStringBuilder(Blackhole blackhole) {
        StringBuffer stringBuffer = new StringBuffer(10);
        int dotCount = (int) (percentage * 10);
        for(int i = 0; i < dotCount; i++) {
            stringBuffer.append("o");
        }
        for(int i = 0; i < 10 - dotCount; i++) {
            stringBuffer.append("O");
        }
        blackhole.consume(stringBuffer.toString());
    }

    private static String getPercentageRounds(double percentage) {
        if(percentage == 0) {
            return "OOOOOOOOOO";
        }
        if(percentage > 0.0 && percentage <= 0.1) {
            return "oOOOOOOOOO";
        }
        if(percentage > 0.1 && percentage <= 0.2) {
            return "ooOOOOOOOO";
        }
        if(percentage > 0.2 && percentage <= 0.3) {
            return "oooOOOOOOO";
        }
        if(percentage > 0.3 && percentage <= 0.4) {
            return "ooooOOOOOO";
        }
        if(percentage > 0.4 && percentage <= 0.5) {
            return "oooooOOOOO";
        }
        if(percentage > 0.5 && percentage <= 0.6) {
            return "ooooooOOOO";
        }
        if(percentage > 0.6 && percentage <= 0.7) {
            return "oooooooOOO";
        }
        if(percentage > 0.7 && percentage <= 0.8) {
            return "ooooooooOO";
        }
        if(percentage > 0.8 && percentage <= 0.9) {
            return "ooooooooo0";
        }
        return "oooooooooo";
    }
}
