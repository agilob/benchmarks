package net.agilob.varia;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Fork(value = 3, jvmArgsAppend = "-Djmh.stack.lines=3")
@Warmup(iterations = 3)
@Measurement(iterations = 10, time = 3, timeUnit = TimeUnit.SECONDS)
public class UUIDGenerator {

    @Benchmark
    public UUID generateUUID() {
        return UUID.randomUUID();
    }

    @Benchmark
    @Fork(jvmArgsAppend = {"-Djava.security.egd=file:/dev/./urandom"})
    public UUID generateUUIDurandom() {
        return UUID.randomUUID();
    }

}
