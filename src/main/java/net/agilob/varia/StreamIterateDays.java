package net.agilob.varia;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static java.time.temporal.ChronoUnit.DAYS;

@Fork(value = 1)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
public class StreamIterateDays {

    private static final LocalDate START_DATE = LocalDate.now().minusDays(1_000);
    private static final LocalDate END_DATE = LocalDate.now().plusDays(1_000);

    @Benchmark
    public List<LocalDate> daysBetweenPeriodOnInts() {
        return IntStream.range(0, (int) DAYS.between(START_DATE, END_DATE) + 1)
                .mapToObj(START_DATE::plusDays)
                .collect(Collectors.toList());
    }

    @Benchmark
    public List<LocalDate> daysBetweenPeriodOnLongs() {
        return LongStream.range(0, DAYS.between(START_DATE, END_DATE) + 1)
                .mapToObj(START_DATE::plusDays)
                .collect(Collectors.toList());
    }

    @Benchmark
    public List<LocalDate> daysBetweenPeriodOnDates() {
        return Stream.iterate(START_DATE, date -> date.plusDays(1))
                .limit(ChronoUnit.DAYS.between(START_DATE, END_DATE))
                .collect(Collectors.toList());
    }

    @Benchmark
    public List<LocalDate> daysBetweenPeriodOnDatesWithLimit() {
        long daysBetween = DAYS.between(START_DATE, END_DATE) + 1L;
        return Stream.iterate(START_DATE, d -> d.plusDays(1))
                .limit(daysBetween)
                .collect(Collectors.toList());
    }
}
