#!/usr/bin/env bash

export RESULTS_PATH='../results'

export JAVA_VERSION='8.0.232-open'
export JAVA_TARGET='1.8'
export OUTPUT_FILE="$RESULTS_PATH/java_1.8_open.json"
export EXCLUDE='**/nine_plus/*' # auto-exclude java 9+ code
JAVA_HOME=~/.sdkman/candidates/java/$JAVA_VERSION mvn clean -Dexcludes=$EXCLUDE --show-version \
    install -Dmaven.compiler.source=$JAVA_TARGET -Djavac.target=$JAVA_TARGET -Dmaven.compiler.target=$JAVA_TARGET 
    ~/.sdkman/candidates/java/$JAVA_VERSION/bin/java -jar target/benchmarks.jar -rff $OUTPUT_FILE -rf json

export JAVA_VERSION='8.0.232.j9-adpt'
export JAVA_TARGET='1.8'
export OUTPUT_FILE="$RESULTS_PATH/java_1.8_j9.json"
export EXCLUDE='**/nine_plus/*' # auto-exclude java 9+ code
JAVA_HOME=~/.sdkman/candidates/java/$JAVA_VERSION mvn clean -Dexcludes=$EXCLUDE --show-version \
    install -Dmaven.compiler.source=$JAVA_TARGET -Djavac.target=$JAVA_TARGET -Dmaven.compiler.target=$JAVA_TARGET 
    ~/.sdkman/candidates/java/$JAVA_VERSION/bin/java -jar target/benchmarks.jar -rff $OUTPUT_FILE -rf json

export EXCLUDE='does_not_exist'
export JAVA_VERSION='9.0.4-open'
export JAVA_TARGET='9'
export OUTPUT_FILE="$RESULTS_PATH/java_9_open.json"
JAVA_HOME=~/.sdkman/candidates/java/$JAVA_VERSION mvn clean -Dexcludes=$EXCLUDE --show-version \
    install -Dmaven.compiler.source=$JAVA_TARGET -Djavac.target=$JAVA_TARGET -Dmaven.compiler.target=$JAVA_TARGET 
    ~/.sdkman/candidates/java/$JAVA_VERSION/bin/java -jar target/benchmarks.jar -rff $OUTPUT_FILE -rf json

export JAVA_VERSION='10.0.2-open'
export JAVA_TARGET='10'
export OUTPUT_FILE="$RESULTS_PATH/java_10_open.json"
JAVA_HOME=~/.sdkman/candidates/java/$JAVA_VERSION mvn clean -Dexcludes=$EXCLUDE --show-version \
    install -Dmaven.compiler.source=$JAVA_TARGET -Djavac.target=$JAVA_TARGET -Dmaven.compiler.target=$JAVA_TARGET 
    ~/.sdkman/candidates/java/$JAVA_VERSION/bin/java -jar target/benchmarks.jar -rff $OUTPUT_FILE -rf json

export JAVA_VERSION='11.0.5-open'
export JAVA_TARGET='11'
export OUTPUT_FILE="$RESULTS_PATH/java_11_open.json"
JAVA_HOME=~/.sdkman/candidates/java/$JAVA_VERSION mvn clean -Dexcludes=$EXCLUDE --show-version \
    install -Dmaven.compiler.source=$JAVA_TARGET -Djavac.target=$JAVA_TARGET -Dmaven.compiler.target=$JAVA_TARGET 
    ~/.sdkman/candidates/java/$JAVA_VERSION/bin/java -jar target/benchmarks.jar -rff $OUTPUT_FILE -rf json

export JAVA_VERSION='11.0.5.j9-adpt'
export JAVA_TARGET='11'
export OUTPUT_FILE="$RESULTS_PATH/java_11_j9.json"
JAVA_HOME=~/.sdkman/candidates/java/$JAVA_VERSION mvn clean -Dexcludes=$EXCLUDE --show-version \
    install -Dmaven.compiler.source=$JAVA_TARGET -Djavac.target=$JAVA_TARGET -Dmaven.compiler.target=$JAVA_TARGET 
    ~/.sdkman/candidates/java/$JAVA_VERSION/bin/java -jar target/benchmarks.jar -rff $OUTPUT_FILE -rf json

export JAVA_VERSION='12.0.2-open'
export JAVA_TARGET='12'
export OUTPUT_FILE="$RESULTS_PATH/java_12_open.json"
JAVA_HOME=~/.sdkman/candidates/java/$JAVA_VERSION mvn clean -Dexcludes=$EXCLUDE --show-version \
    install -Dmaven.compiler.source=$JAVA_TARGET -Djavac.target=$JAVA_TARGET -Dmaven.compiler.target=$JAVA_TARGET 
    ~/.sdkman/candidates/java/$JAVA_VERSION/bin/java -jar target/benchmarks.jar -rff $OUTPUT_FILE -rf json

export JAVA_VERSION='12.0.2.j9-adpt'
export JAVA_TARGET='12'
export OUTPUT_FILE="$RESULTS_PATH/java_12_j9.json"
JAVA_HOME=~/.sdkman/candidates/java/$JAVA_VERSION mvn clean -Dexcludes=$EXCLUDE --show-version \
    install -Dmaven.compiler.source=$JAVA_TARGET -Djavac.target=$JAVA_TARGET -Dmaven.compiler.target=$JAVA_TARGET 
    ~/.sdkman/candidates/java/$JAVA_VERSION/bin/java -jar target/benchmarks.jar -rff $OUTPUT_FILE -rf json

export JAVA_VERSION='13.0.1-open'
export JAVA_TARGET='13'
export OUTPUT_FILE="$RESULTS_PATH/java_13_open.json"
JAVA_HOME=~/.sdkman/candidates/java/$JAVA_VERSION mvn clean -Dexcludes=$EXCLUDE --show-version \
    install -Dmaven.compiler.source=$JAVA_TARGET -Djavac.target=$JAVA_TARGET -Dmaven.compiler.target=$JAVA_TARGET 
    ~/.sdkman/candidates/java/$JAVA_VERSION/bin/java -jar target/benchmarks.jar -rff $OUTPUT_FILE -rf json

export JAVA_VERSION='13.0.1.j9-adpt'
export JAVA_TARGET='13'
export OUTPUT_FILE="$RESULTS_PATH/java_13_j9.json"
JAVA_HOME=~/.sdkman/candidates/java/$JAVA_VERSION mvn clean -Dexcludes=$EXCLUDE --show-version \
    install -Dmaven.compiler.source=$JAVA_TARGET -Djavac.target=$JAVA_TARGET -Dmaven.compiler.target=$JAVA_TARGET 
    ~/.sdkman/candidates/java/$JAVA_VERSION/bin/java -jar target/benchmarks.jar -rff $OUTPUT_FILE -rf json

export JAVA_VERSION='19.2.1-grl'
export JAVA_TARGET='11'
export OUTPUT_FILE="$RESULTS_PATH/java_11_graal.json"
JAVA_HOME=~/.sdkman/candidates/java/$JAVA_VERSION mvn clean -Dexcludes=$EXCLUDE --show-version \
    install -Dmaven.compiler.source=$JAVA_TARGET -Djavac.target=$JAVA_TARGET -Dmaven.compiler.target=$JAVA_TARGET 
    ~/.sdkman/candidates/java/$JAVA_VERSION/bin/java -jar target/benchmarks.jar -rff $OUTPUT_FILE -rf json
